﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixOperations
{
    class Matrix
    {
        private int sizeM = 0;
        public int SizeM
        {
            get
            {
                return sizeM;
            }
            set
            {
                sizeM = value;
            }
        }


        private int sizeN = 0;
        public int SizeN
        {
            get
            {
                return sizeN;
            }
            set
            {
                sizeN = value;
            }
        }
        public MatrixElement[,] matrixMassive;
        public Matrix(int sizeM, int sizeN)
        {
            SizeM = sizeM;
            SizeN = sizeN;
           /* if(SizeM <= 0 || SizeN <= 0)
            {
                
            }*/
            matrixMassive = new MatrixElement[sizeM , sizeN];
        }

        static public void SetValueOfElements(Matrix matrix)
        {

            for(int i = 0; i < matrix.SizeM; i++)
            {
                for(int j = 0; j < matrix.SizeN; j++)
                {
                    Console.WriteLine("Введите значение элемента в позиции {0}х{1}", i + 1, j + 1);
                    int value = Int32.Parse(Console.ReadLine());
                    matrix.matrixMassive[i,j] = new MatrixElement(i + 1, j + 1, value);
                }
            }
        }
        static public void PrintMatrix(Matrix matrix)
        {
            string result = "";
            for(int i = 0; i < matrix.SizeM; i++)
            {
                for (int j = 0; j < matrix.SizeN; j++)
                {
                    result += matrix.matrixMassive[i, j].ValueOfElement + "\t";
                    if(j == matrix.SizeN - 1)
                    {
                        result += "\n";
                    }
                }
            }
            Console.WriteLine(result);
        }
        static public Matrix SumOfMatrixes(Matrix matrix1, Matrix matrix2)
        {
            Matrix sumOfMatrixes = new Matrix(matrix1.SizeM, matrix2.SizeN);
            for(int i = 0; i < sumOfMatrixes.SizeM; i++)
                {
                    for(int j = 0; j<sumOfMatrixes.SizeN; j++)
                    {
                        sumOfMatrixes.matrixMassive[i, j] = new MatrixElement(i + 1, j + 1, matrix1.matrixMassive[i, j].ValueOfElement + matrix2.matrixMassive[i, j].ValueOfElement);
                    }
                }
                return sumOfMatrixes;
           
        }
        static public Matrix MultipleOnNumber(Matrix matrix, int number)
        {
            for(int i = 0; i<matrix.SizeM; i++)
            {
                for(int j = 0; j < matrix.SizeN; j++)
                {
                    matrix.matrixMassive[i, j].ValueOfElement *= number;
                }
            }
            return matrix;
        }

        static public string MatrixCompare(Matrix matrix1, Matrix matrix2)
        {
            string result = "";
            if
        }
    }
}
