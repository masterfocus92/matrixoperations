﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixOperations
{
    class MatrixElement
    {
        int indexM;
        int indexN;
        int valueOfElement;
        public int ValueOfElement
        {
            get
            {
                return valueOfElement;
            }
            set
            {
                valueOfElement = value;
            }
        }

        public MatrixElement(int m, int n, int value)
        {
            indexM = m;
            indexN = n;
            valueOfElement = value;
        }
    }
}
